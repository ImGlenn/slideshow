﻿#region copyright
/*    
	Copyright (C) 2018  Glenn

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#endregion
using System;
using System.Windows.Forms;

namespace Slideshow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.Hide();
            label1.Text = "1";
            var image1 = Slideshow.Properties.Resources._1;
            panel1.BackgroundImage = image1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var image1 = Slideshow.Properties.Resources._1;
            var image2 = Slideshow.Properties.Resources._2;
            var image3 = Slideshow.Properties.Resources._3;
            var image4 = Slideshow.Properties.Resources._4;
            var image5 = Slideshow.Properties.Resources._5;
            var image6 = Slideshow.Properties.Resources._6;

            if (label1.Text == "1")
            {
                label1.Text = "2";
                panel1.BackgroundImage = image2;
            }
            else
            {
                if (label1.Text == "2")
                {
                    label1.Text = "3";
                    panel1.BackgroundImage = image3;
                }
                else
                {
                    if (label1.Text == "3")
                    {
                        label1.Text = "4";
                        panel1.BackgroundImage = image4;
                    }
                    else
                    {
                        if (label1.Text == "4")
                        {
                            label1.Text = "5";
                            panel1.BackgroundImage = image5;
                        }
                        else
                        {
                            if (label1.Text == "5")
                            {
                                label1.Text = "6";
                                panel1.BackgroundImage = image6;
                            }
                            else
                            {
                                if (label1.Text == "6")
                                {
                                    label1.Text = "1";
                                    panel1.BackgroundImage = image1;
                                }
                            }
                        }
                        }
                    }
                }
            }

        private void button1_Click(object sender, EventArgs e)
        {
            var image1 = Slideshow.Properties.Resources._1;
            var image2 = Slideshow.Properties.Resources._2;
            var image3 = Slideshow.Properties.Resources._3;
            var image4 = Slideshow.Properties.Resources._4;
            var image5 = Slideshow.Properties.Resources._5;
            var image6 = Slideshow.Properties.Resources._6;
            if (label1.Text == "1")
            {
                label1.Text = "6";
                panel1.BackgroundImage = image6;
            }
            else
            {
                if (label1.Text == "6")
                {
                    label1.Text = "5";
                    panel1.BackgroundImage = image5;
                }
                else
                {
                    if (label1.Text == "5")
                    {
                        label1.Text = "4";
                        panel1.BackgroundImage = image4;
                    }
                    else
                    {
                        if (label1.Text == "4")
                        {
                            label1.Text = "3";
                                panel1.BackgroundImage = image3;
                        }
                        else
                        {
                            if (label1.Text == "3")
                            {
                                label1.Text = "2";
                                panel1.BackgroundImage = image2;
                            }
                            else
                            {
                                if (label1.Text == "2")
                                {
                                    label1.Text = "1";
                                        panel1.BackgroundImage = image1;
                                }
                            }
                        }
                    }
                }

            }
        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.BackgroundImageLayout = ImageLayout.Center;
        }

        private void noneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.BackgroundImageLayout = ImageLayout.None;
        }

        private void stretchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void tileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.BackgroundImageLayout = ImageLayout.Tile;
        }

        private void zoomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.BackgroundImageLayout = ImageLayout.Zoom;
        }
    }
}
