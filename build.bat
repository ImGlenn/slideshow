@ECHO off
cls
:start
ECHO.
ECHO x86 = 32-bit
ECHO x64 = 64-bit
ECHO 1. Build x86 Platform
ECHO 2. Build x64 Platform
ECHO 3. Build both x86 and x64 Platform
ECHO 4. Run x86 Platform
ECHO 5. Run x64 Platform
ECHO 6. Run both x86 and x64 Platform
ECHO 7. Clean
ECHO 8. Exit
set choice=
set /p choice= Type the number of your choice.  
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto x86
if '%choice%'=='2' goto x64
if '%choice%'=='3' goto x
if '%choice%'=='4' goto r86
if '%choice%'=='5' goto r64
if '%choice%'=='6' goto r
if '%choice%'=='7' goto clean
if '%choice%'=='8' goto exit
ECHO "%choice%" is not valid, try again
ECHO.
goto start
:x86
call MSBuild.exe Slideshow.sln /p:Configuration=BUILD /p:Platform="x86"
goto end
:x64
call MSBuild.exe Slideshow.sln /p:Configuration=BUILD /p:Platform="x64"
goto end
:x
call MSBuild.exe Slideshow.sln /p:Configuration=BUILD /p:Platform="x86"
call MSBuild.exe Slideshow.sln /p:Configuration=BUILD /p:Platform="x64"
goto end
:c86
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x86"
goto end
:c64
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x64"
goto end
:c
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x86"
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x64"
goto end
:r86
start Slideshow\bin\BUILD\x86\Slideshow.exe
goto end
:r64
start Slideshow\bin\BUILD\x64\Slideshow.exe
goto end
:r
start Slideshow\bin\BUILD\x86\Slideshow.exe
start Slideshow\bin\BUILD\x64\Slideshow.exe
goto end
:exit
EXIT /B
:clean
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x86"
call MSBuild.exe Slideshow.sln /t:Clean /p:Configuration=BUILD /p:Platform="x64"
goto end
:end
pause